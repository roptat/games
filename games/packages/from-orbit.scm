;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages from-orbit)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games game-local-fetch)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public from-orbit
  (package
    (name "from-orbit")
    (version "0")
    (source (origin
              (method game-local-fetch)
              (uri "from-orbit-linux-x86.zip")
              (sha256
               (base32
                "0qqcwzwnrf2111clv5gcch9sgnx6v9b37wzm1xvd77zmqa0pdhrw"))))
    (supported-systems '("i686-linux"))
    (build-system binary-build-system)
    (arguments
     `(#:patchelf-plan
       `(("FromOrbit.x86"
          ("libc" "gcc:lib" "eudev" "gdk-pixbuf" "glib" "gtk+-2" "libx11"
           "libxcursor" "libxext" "libxi" "libxinerama" "libxrandr" "libxscrnsaver"
           "libxxf86vm" "mesa" "pulseaudio" "zlib"))
         ("FromOrbit_Data/Plugins/x86/ScreenSelector.so"
          ("libc" "gcc:lib" "gtk+-2")))
       #:install-plan
       `(("FromOrbit.x86" "share/from-orbit/")
         ("FromOrbit_Data" "share/from-orbit/"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'chdir
           (lambda _
             ;; unpack enters the first subdirectory, but the archive doesn't
             ;; have one that contains everything.
             (chdir "..")
             #t))
         (add-after 'install 'make-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (wrapper (string-append out "/bin/from-orbit"))
                    (binary (string-append out "/share/from-orbit/FromOrbit.x86")))
               (make-wrapper
                 wrapper binary
                 #:skip-argument-0? #t
                 `("PATH" ":" prefix
                   (,(string-append (assoc-ref inputs "pulseaudio") "/bin")))))
             #t))
         (add-after 'install 'make-desktop-entry
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (make-desktop-entry-file
                 (string-append out "/share/applications/from-orbit.desktop")
                 #:name "From Orbit"
                 #:exec (string-append out "/bin/from-orbit")
                 #:categories '("Application" "Game")))
             #t)))))
    (inputs
     `(("eudev" ,eudev)
       ("gcc:lib" ,gcc "lib")
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("glib" ,glib)
       ("gtk+-2" ,gtk+-2)
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("libxi" ,libxi)
       ("libxinerama" ,libxinerama)
       ("libxrandr" ,libxrandr)
       ("libxscrnsaver" ,libxscrnsaver)
       ("libxxf86vm" ,libxxf86vm)
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)
       ("zlib" ,zlib)))
    (native-inputs
     `(("unzip" ,unzip)))
    (home-page "https://tentaclehead.itch.io/from-orbit")
    (synopsis "Resource gathering game")
    (description "In From Orbit you'll manage the crew of a small spaceship.
Stranded deep in uncharted space, they need to make their way home.  Journey
from planet to planet, each procedurally generated with unique environments,
challenges, and alien creatures.

Scout out the richest resources and the deadliest critters, and discover
unique challenges and powerful artifacts. 

You're going to need all the ore and crystal you can carry if you're going to
mount a successful defense or purchase the upgrades you'll need to make it
home alive. 

Err… Escape.  The resources on each planet won't last forever, but the alien
creatures willing to bite your legs off never seem to end, eventually
overwhelming even the most stalwart crew.  Hold out as long as you can… but
don't throw your resources — or the lives of your crew — away to a lost cause.
Always be prepared to make a timely exit when things start getting hairy.")
    (license (undistributable "No license"))))
