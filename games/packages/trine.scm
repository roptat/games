;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages trine)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (nongnu packages game-development)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages image)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (games humble-bundle))

(define-public glew-1                   ; TODO: Last GLEW with glewinfo / visualinfo binaries?  Move upstream?
  (package
    (inherit glew)
    (version "1.5.8")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://sourceforge/glew/glew/" version
                                  "/glew-" version ".tgz"))
              (sha256
               (base32
                "1125xwvg424c5kfnhkw69smazcriwwhd04z8fab6zzvgs6xwrz9k"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  (substitute* "config/Makefile.linux"
                    (("= cc") "= gcc")
                    (("/lib64") "/lib"))
                  #t))))
    (arguments
     (substitute-keyword-arguments (package-arguments glew)
       ((#:phases phases)
        `(modify-phases ,phases
           (add-after 'unpack 'fix-makefile
             (lambda* (#:key outputs #:allow-other-keys)
               (substitute* "config/Makefile.linux"
                 (("-L/usr/X11R6/lib")
                  (string-append "-Wl,-rpath=" (assoc-ref outputs "out") "/lib")))))))))))

(define-public trine
  ;; Also see https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=trine.
  (let* ((version "4")
         (arch (match (or (%current-target-system)
                          (%current-system))
                 ("x86_64-linux" "64")
                 ("i686-linux" "32")
                 (_ "")))
         (other-arch (match (or (%current-target-system)
                                (%current-system))
                       ("x86_64-linux" "32")
                       ("i686-linux" "64")
                       (_ "")))
         (file-name (string-append "TrineUpdate" version "." arch ".run")))
    (package
      (name "trine")
      (version "1.08")
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           (match (or (%current-target-system)
                      (%current-system))
             ("x86_64-linux"
              "1ycqj8wbpybbyvcnqsn1ysal9r68ihfb4y31f8nb1vrv4ddciv2h")
             ("i686-linux"
              "0hzkbqw3yqlh0q8lcaa2qwl92j8ggwipajz8lm2qs98a7gqbsr6x")
             (_ ""))))))
      (build-system binary-build-system)
      (supported-systems '("x86_64-linux" "i686-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,(string-append "trine-launcher" arch)
            ("gcc" "gtk" "libglade" "glib" "sdl" "out"))
           (,,(string-append "trine-bin" arch)
            ("gcc" "glew" "glib" "glu" "gtk" "libvorbis" "mesa" "nvidia-cg-toolkit"
             "openal" "sdl" "sdl-image" "sdl-ttf" "zlib" "out"))
           ;; ffmpeg
           (,,(string-append "lib" arch "/libavcodec.so.52")
            ("out"))
           (,,(string-append "lib" arch "/libavformat.so.52")
            ("out"))
           (,,(string-append "lib" arch "/libswscale.so.0")
            ("out"))
           ;; boost
           (,,(string-append "lib" arch "/libboost_filesystem.so.1.35.0")
            ("out" "gcc"))
           (,,(string-append "lib" arch "/libboost_regex.so.1.35.0")
            ("out" "gcc"))
           (,,(string-append "lib" arch "/libboost_system.so.1.35.0")
            ("gcc"))
           (,,(string-append "lib" arch "/libboost_thread-mt.so.1.35.0")
            ("gcc"))
           ;; icu
           (,,(string-append "lib" arch "/libicui18n.so.38")
            ("out" "gcc"))
           (,,(string-append "lib" arch "/libicuuc.so.38")
            ("out" "gcc"))
           ;; opencv
           (,,(string-append "lib" arch "/libcxcore.so.1")
            ("gcc"))
           (,,(string-append "lib" arch "/libcv.so.1")
            ("gcc" "out"))
           ;; PhysX:
           (,,(string-append "lib" arch "/libPhysXCore.so")
            ("gcc"))
           (,,(string-append "lib" arch "/libPhysXLoader.so.1")
            ("gcc"))
           (,,(string-append "lib" arch "/libNxCooking.so")
            ("gcc")))
         #:install-plan
         `(("." "share/trine"
            #:exclude (,,(string-append "trine-bin" other-arch)
                       ,,(string-append "trine-launcher" other-arch)
                       "trine-bin"
                       "trine-launcher"
                       "createShortcuts.sh"
                       "removeShortcuts.sh"
                       "environment-variables")
            #:exclude-regexp ("lib32" "lib64"))
           (,,(string-append "lib" arch) "lib"
            #:include ("libPhysXLoader.so.1"
                       "libPhysXCore.so"
                       "libNxCooking.so"
                       ;; TODO: Build ffmpeg < 3.4?
                       "libavcodec.so.52"
                       "libavformat.so.52"
                       "libavutil.so.50"
                       "libswscale.so.0"
                       ;; TODO: Build boost 1.35?
                       "libboost_filesystem.so.1.35.0"
                       "libboost_regex.so.1.35.0"
                       "libboost_system.so.1.35.0"
                       "libboost_thread-mt.so.1.35.0"
                       ;; TODO: Build OpenCV2?
                       "libcv.so.1"
                       "libcxcore.so.1"
                       ;; TODO: Build icu4c .8?
                       "libicudata.so.38"
                       "libicui18n.so.38"
                       "libicuuc.so.38")))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               ;; We use system* to ignore errors.
               (system* (which "unzip")
                        (assoc-ref inputs "source"))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (lib (string-append output "/lib"))
                      (wrapper (string-append output "/bin/trine"))
                      (real (string-append output "/share/trine/trine-bin" ,arch))
                      (real-noarch (string-append output "/share/trine/trine-bin"))
                      (launcher (string-append output "/bin/trine-launcher"))
                      (real-launcher (string-append
                                      output "/share/trine/trine-launcher" ,arch))
                      (icon (string-append output "/share/trine/trine.xpm")))
                 (make-wrapper wrapper real
                               #:skip-argument-0? #t
                               `("LD_LIBRARY_PATH" ":" prefix
                                 ,(list lib)))
                 (symlink real-launcher launcher)
                 (symlink wrapper real-noarch) ; The launcher looks for "trine-bin".
                 (make-desktop-entry-file (string-append
                                           output
                                           "/share/applications/"
                                           "trine.desktop")
                                          #:name "Trine"
                                          #:exec launcher
                                          #:icon icon
                                          #:comment "Side-scrolling, action platform-puzzle video game"
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc" ,gcc "lib")
         ("glew" ,glew-1)
         ("glib" ,glib)
         ("glu" ,glu)
         ("gtk" ,gtk+-2)
         ("libglade" ,libglade)
         ("libvorbis" ,libvorbis)
         ("mesa" ,mesa)
         ("nvidia-cg-toolkit" ,nvidia-cg-toolkit)
         ("openal" ,openal)
         ("sdl" ,sdl)
         ("sdl-image" ,sdl-image)
         ("sdl-ttf" ,sdl-ttf)
         ("zlib" ,zlib)))
      (home-page "http://www.frozenbyte.com/games/trine/")
      (synopsis "Side-scrolling, action platform-puzzle video game")
      (description "Trine is a fantasy action platformer with a fairytale-like
atmosphere, following the journey of three heroes – a Wizard, a Knight and a
Thief – in a quest to save the kingdom from evil.  The gameplay is based on
fully interactive physics – each character's different abilities and tactics
can be used to invent new ways to battle an army of undead and overcome
obstacles, and restore the balance of the world.")
      (license (undistributable "No URL")))))

;; TODO: Pulseaudio fails on x86_64 with:
;; ALSA lib pcm_dmix.c:1089:(snd_pcm_dmix_open) unable to open slave
;; ALSA lib pcm.c:2642:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.rear
;; ALSA lib pcm.c:2642:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.center_lfe
;; ALSA lib pcm.c:2642:(snd_pcm_open_noupdate) Unknown PCM cards.pcm.side
;; ALSA lib pcm_route.c:869:(find_matching_chmap) Found no matching channel map
;; Expression 'GetExactSampleRate( hwParams, &defaultSr )' failed in 'src/hostapi/alsa/pa_linux_alsa.c', line: 872
;; ALSA lib pcm_dmix.c:1024:(snd_pcm_dmix_open) The dmix plugin supports only playback stream
;; ALSA lib pcm_dmix.c:1089:(snd_pcm_dmix_open) unable to open slave
;; ALSA lib dlmisc.c:285:(snd_dlobj_cache_get0) Cannot open shared library /gnu/store/2fj86yvbjfg6i5wd0cgs9az8371j10p2-alsa-plugins-1.2.1-pulseaudio/lib/alsa-lib/libasound_module_pcm_pulse.so ((null): /gnu/store/2fj86yvbjfg6i5wd0cgs9az8371j10p2-alsa-plugins-1.2.1-pulseaudio/lib/alsa-lib/libasound_module_pcm_pulse.so: wrong ELF class: ELFCLASS64)
;; ALSA lib dlmisc.c:285:(snd_dlobj_cache_get0) Cannot open shared library /gnu/store/2fj86yvbjfg6i5wd0cgs9az8371j10p2-alsa-plugins-1.2.1-pulseaudio/lib/alsa-lib/libasound_module_pcm_pulse.so ((null): /gnu/store/2fj86yvbjfg6i5wd0cgs9az8371j10p2-alsa-plugins-1.2.1-pulseaudio/lib/alsa-lib/libasound_module_pcm_pulse.so: wrong ELF class: ELFCLASS64)
;; ALSA lib dlmisc.c:285:(snd_dlobj_cache_get0) Cannot open shared library /gnu/store/2fj86yvbjfg6i5wd0cgs9az8371j10p2-alsa-plugins-1.2.1-pulseaudio/lib/alsa-lib/libasound_module_pcm_pulse.so ((null): /gnu/store/2fj86yvbjfg6i5wd0cgs9az8371j10p2-alsa-plugins-1.2.1-pulseaudio/lib/alsa-lib/libasound_module_pcm_pulse.so: wrong ELF class: ELFCLASS64)
;;
;; Game still works though, but this may be the reason why it hangs for so long before starting.

(define-public trine-enchanted-edition
  (let* ((version "2.12")
         (build "508")
         (file-name (string-append "trine_enchanted_edition_"
                                   "v" (string-replace-substring version "." "_")
                                   "_build_" build
                                   "_humble_linux_full.zip")))
    (package
      (name "trine-enchanted-edition")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "06kvxk7bq0pl9iw5694h8agfh1mx0w5pbzsz0ppwvkzvcigfj4pj"))))
      (build-system binary-build-system)
      (supported-systems '("x86_64-linux" "i686-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         '(("_enchanted_edition_/bin/trine1_linux_launcher_32bit"
            ("gcc" "gtk" "gdk-pixbuf" "pango" "glib" "libpng" "libx11"))
           ("_enchanted_edition_/bin/trine1_linux_32bit"
            ("nvidia-cg-toolkit" "libogg" "mesa" "glu" "freetype" "zlib" "gcc"
             "libasound")))
         #:install-plan
         `(("_enchanted_edition_" "share/trine-enchanted-edition"
            #:exclude-regexp ("lib/lib32")
            #:exclude ("trine1_bin_starter.sh"
                       "install_dependencies_ubuntu_64bit.sh"
                       "trine1.sh")))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/trine-enchanted-edition"))
                      (real (string-append output "/share/trine-enchanted-edition/bin/"
                                           "trine1_linux_launcher_32bit"))
                      (icon (string-append output "/share/trine-enchanted-edition/"
                                           "trine1.png")))
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a/bin/bash~%" (which "bash"))
                     (format #t "cd ~a/..~%" (dirname real))
                     (format #t "exec -a \"$0\" \"~a\" \"$@\"" real)))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file (string-append
                                           output
                                           "/share/applications/"
                                           "trine-enchanted-edition.desktop")
                                          #:name "Trine Enchanted Edition"
                                          #:exec wrapper
                                          #:icon icon
                                          #:comment "Side-scrolling, action platform-puzzle video game"
                                          #:categories '("Application" "Game")))
               #t))
           (add-after 'install 'make-installer-startup-script
             ;; The launcher calls this script to start the game.
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/share/trine-enchanted-edition/"
                                              "bin/trine1_bin_starter.sh"))
                      (eudev (string-append (assoc-ref inputs "eudev") "/lib")))
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a~%" (which "bash"))
                     (format #t "cd \"~a\"/..~%" (dirname wrapper))
                     ;; Needed, otherwise the game fails to load SDL.
                     (format #t "export LD_LIBRARY_PATH=\"~a\"~%" eudev)
                     (format #t "./bin/trine1_linux_32bit~%")))
                 (chmod wrapper #o755))
               #t))
           (add-after 'install 'make-executable
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (bin (string-append output "/share/trine-enchanted-edition/bin/")))
                 (chmod (string-append bin "/trine1_linux_32bit") #o755)
                 (chmod (string-append bin "/trine1_linux_launcher_32bit") #o755))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc" ,gcc "lib")
         ("nvidia-cg-toolkit" ,nvidia-cg-toolkit)
         ("mesa" ,mesa)
         ("glu" ,glu)
         ("sdl" ,sdl)
         ("openal" ,openal)
         ("libvorbis" ,libvorbis)
         ("gtk" ,gtk+-2)
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("pango" ,pango)
         ("glib" ,glib)
         ("libx11" ,libx11)
         ("libpng" ,libpng-1.2)
         ("freetype" ,freetype)
         ("libogg" ,libogg)
         ("zlib" ,zlib)
         ("libasound" ,alsa-lib)
         ;; Loaded at runtime:
         ("eudev" ,eudev)))
      (home-page "http://www.frozenbyte.com/games/trine-enchanted-edition")
      (synopsis "Side-scrolling, action platform-puzzle video game")
      (description "Trine is a fantasy action platformer with a fairytale-like
atmosphere, following the journey of three heroes – a Wizard, a Knight and a
Thief – in a quest to save the kingdom from evil.  The gameplay is based on
fully interactive physics – each character's different abilities and tactics
can be used to invent new ways to battle an army of undead and overcome
obstacles, and restore the balance of the world.


The Echanted Edition is a port of the original game to the Trine 2 engine and
adds multiplayer.

Note: The game may hang for a few seconds before starting.")
      (license (undistributable "No URL")))))
