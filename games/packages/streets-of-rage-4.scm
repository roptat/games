;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages streets-of-rage-4)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages video)
  #:use-module (games utils)
  #:use-module (nonguix build utils)
  #:use-module (games humble-bundle)
  #:use-module (nonguix licenses))

(define-public gog-streets-of-rage-4
  (let ((buildno "42672"))
    (package
      (name "gog-streets-of-rage-4")
      (version "05g-r11299")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://streets_of_rage_4/en3installer0")
         (file-name (string-append "streets_of_rage_4"
                                   (string-replace-substring version "-" "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32
           "046dnbl46rzrbc6mh3bddmhpkxr05kwnyzgb659xis9rl1ryzsc3"))))
      (supported-systems '("x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(;; SDL2 is found at runtime anyways, so no need to add it to the patchelf plan.
         #:patchelf-plan
         `(("SOR4"
            ("gcc:lib"))
           ("lib64/libFAudio.so.0")
           ("lib64/libFNA3D.so.0")
           ("lib64/libGogGalaxyGCWrapper.so"
            ("gcc:lib" "libstdc++"))
           ("lib64/libMonoPosixHelper.so"
            ("zlib"))
           ("lib64/libWwise.so"
            ("gcc:lib" "libstdc++")))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             ;; The game is too big (> 4 GiB) to be reasonably extracted to /tmp.
             (lambda* (#:key inputs #:allow-other-keys)
               ;; Use `system*' to ignore errors.
               (system* "unzip"
                        (assoc-ref inputs "source")
                        "data/noarch/game/SOR4"
                        "data/noarch/game/lib64/libFAudio.so.0"
                        "data/noarch/game/lib64/libFNA3D.so.0"
                        "data/noarch/game/lib64/libGalaxy64.so"
                        "data/noarch/game/lib64/libGogGalaxyGCWrapper.so"
                        "data/noarch/game/lib64/libMonoPosixHelper.so"
                        "data/noarch/game/lib64/libWwise.so")
               ;; Required by mojo-build-system:
               (system* "unzip"
                        (assoc-ref inputs "source")
                        "data/noarch/gameinfo")
               #t))
           (add-before 'install 'extract-resource
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/"
                                            ,name "-" ,version))
                      (share-temp (string-append share "-temp")))
                 (mkdir-p share)
                 ;; Use `system*' to ignore errors.
                 (system* "unzip"
                          (assoc-ref inputs "source")
                          "-d" share-temp)
                 (rename-file (string-append share-temp "/data/noarch")
                              share)
                 (delete-file-recursively share-temp)
                 #t)))
           (add-after 'install 'replace-embedded-libs
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/"
                                            ,name "-" ,version
                                            "/game/lib64/")))
                 (for-each
                  (lambda (pkg+lib)
                    (let ((target (string-append share "/" (cadr pkg+lib))))
                      (delete-file target)
                      (symlink (string-append (car pkg+lib) "/lib/" (cadr pkg+lib))
                               target)))
                  `((,(assoc-ref inputs "harfbuzz") "libharfbuzz.so")
                    (,(assoc-ref inputs "theorafile") "libtheorafile.so")
                    (,(assoc-ref inputs "sdl2") "libSDL2-2.0.so.0")))
                 #t)))
           (add-after 'install 'wrap-executable
             ;; Game segfaults if the "game/data/texture*" files are not
             ;; writable.
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (share (string-append out "/share/"
                                            ,name "-" ,version))
                      (wrapper (string-append out "/bin/streets-of-rage-4")))
                 (delete-file (string-append out "/bin/streets-of-rage"))
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (which "bash") "\n"
                              "[ -n \"$XDG_DATA_HOME\" ] || XDG_DATA_HOME=\"$HOME/.local/share\"" "\n"
                              "if [ ! -e \"$XDG_DATA_HOME/streets-of-rage-4\" ]; then" "\n"
                              "  echo \"Initializing... (This may take a while.)\"" "\n"
                              "  mkdir -p \"$XDG_DATA_HOME/streets-of-rage-4/data\"" "\n"
                              "  cp " share "/game/SOR4 \"$XDG_DATA_HOME/streets-of-rage-4/\"" "\n"
                              "  for i in " share "/game/* ; do" "\n"
                              "    ln -s \"$i\" \"$XDG_DATA_HOME/streets-of-rage-4/\"" "\n"
                              "  done" "\n"
                              "  cp " share "/game/data/texture* \"$XDG_DATA_HOME/streets-of-rage-4/data/\"" "\n"
                              "  chmod +w \"$XDG_DATA_HOME\"/streets-of-rage-4/data/texture*" "\n"
                              "  for i in " share "/game/data/* ; do" "\n"
                              "    ln -s \"$i\" \"$XDG_DATA_HOME/streets-of-rage-4/data/\"" "\n"
                              "  done" "\n"
                              "fi" "\n"
                              "cd \"$XDG_DATA_HOME/streets-of-rage-4\"" "\n"
                              "export LD_LIBRARY_PATH=\"lib64/${LD_LIBRARY_PATH=:+:}$LD_LIBRARY_PATH\"" "\n"
                              "exec -a SOR4 ./SOR4 \"$@\"" "\n"))))
                 (chmod wrapper #o755)
                 ;; We take the opportunity to fix the desktop file which
                 ;; points to the wrong name.
                 (let ((desktop-file (string-append out "/share/applications/"
                                                    "streets-of-rage-4.desktop")))
                   (rename-file (string-append out "/share/applications/"
                                               "streets-of-rage.desktop")
                                desktop-file)
                   (substitute* desktop-file
                     (("Exec=.*") (string-append "Exec=" wrapper "\n"))))
                 #t))))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("harfbuzz" ,harfbuzz)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("sdl2" ,sdl2)
         ("theorafile" ,theorafile)
         ("zlib" ,zlib)))
      (home-page "https://www.streets4rage.com/")
      (synopsis "Side-scrolling beat 'em up")
      (description "Carrying on the style of gameplay from previous entries in
the Streets of Rage series, Streets of Rage 4 is a side-scrolling beat 'em up
in which up to four players locally or two players online fight against waves
of enemies, aided by disposable weapons and item pickups.  Alongside standard
attacks, throws, and Blitz Moves, each character has a set of special attacks
that can be performed at the cost of some health.  In this game, however,
players can restore health spent on a special attack by performing successive
follow-up attacks without getting hit.  Each character also has a unique \"Star
Move\", which can be performed by collecting Stars in each level.  A new combo
system is introduced, along with the ability to juggle opponents against walls
and other players, allowing players to earn extra points by stringing together
long combos without getting hit.")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
