;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages mini-metro)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (games utils)
  #:use-module (games humble-bundle))

(define-public mini-metro
  (let ((binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "Mini Metro.x86_64")
                  ("i686-linux"   "Mini Metro.x86"))))
    (package
      (name "mini-metro")
      (version "41")
      (source (origin
               (method humble-bundle-fetch)
               (uri (humble-bundle-reference
                      (help (humble-bundle-help-message name))
                      (config-path '(mini-metro key))))
               (file-name "MiniMetro-release-41-linux.tar.gz")
               (sha256
                (base32
                 "0c9aksdxkkhad3ppc2bpd38f933bq27slbmskbavh20i5njn8pq2"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("dbus" "eudev" "gcc:lib" "libc" "libxcursor" "libxi" "libxinerama"
             "libxrandr" "libxscrnsaver" "mesa" "pulseaudio")))
         #:install-plan
         `((,,binary "share/mini-metro/")
           ("Mini Metro_Data" "share/mini-metro/")
           ("bin/mini-metro" "bin/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'chdir
             (lambda _
               (chdir "..")
               #t))
           (add-before 'install 'create-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (mkdir "bin")
               (let ((metro-wrapper "bin/mini-metro"))
                 (with-output-to-file metro-wrapper
                   (lambda _
                     (format #t "#!~a~%" (which "bash"))
                     (format #t "cd ~a~%" (string-append (assoc-ref outputs "out")
                                                         "/share/mini-metro"))
                     (format #t "./\"~a\"~%" ,binary)))
                 (chmod metro-wrapper #o755))
               (chmod ,binary #o755)
               #t)))))
      (inputs
       `(("dbus" ,dbus)
         ("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("libxcursor" ,libxcursor)
         ("libxinerama" ,libxinerama)
         ("libxi" ,libxi)
         ("libxrandr" ,libxrandr)
         ("libxscrnsaver" ,libxscrnsaver)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)))
      (home-page "https://dinopoloclub.com/games/mini-metro")
      (synopsis "Metro management simulation game")
      (description "Mini Metro is a strategy simulation game about designing a
subway map for a growing city.  Draw lines between stations and start your
trains running.  As new stations open, redraw your lines to keep them
efficient.  Decide where to use your limited resources.  How long can you keep
the city moving?")
      (license (undistributable "No URL")))))
