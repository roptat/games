;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages beglitched)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games game-local-fetch)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public beglitched
  (let ((binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "Beglitched.x86_64")
                  (_              "Beglitched.x86"))))
    (package
      (name "beglitched")
      (version "1.01")
      (source
       (origin
        (method game-local-fetch)
        (uri "Beglitched_Linux_v1.01.zip")
        (sha256
         (base32
          "182g9bdsyn9l8bqi86105ban82b4qfaq6nwg74zg44m9y530cpz3"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system binary-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "gcc:lib" "eudev" "gdk-pixbuf" "glib" "glu" "gtk+-2" "libx11"
             "libxcursor" "libxext" "libxi" "libxinerama" "libxrandr" "libxscrnsaver"
             "libxxf86vm" "mesa" "pulseaudio" "zlib"))
           ("Beglitched_Data/Plugins/x86/ScreenSelector.so"
            ("libc" "gcc:lib" "gtk+-2")))
         #:install-plan
         `((,,binary "share/beglitched/")
           ("Beglitched_Data" "share/beglitched/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'chdir
             (lambda _
               ;; unpack enters the first subdirectory, but the archive doesn't
               ;; have one that contains everything.
               (chdir "..")
               #t))
           (add-after 'install 'make-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (wrapper (string-append out "/bin/beglitched"))
                      (binary (string-append out "/share/beglitched/" ,binary)))
                 (make-wrapper
                   wrapper binary
                   `("PATH" ":" prefix
                     (,(string-append (assoc-ref inputs "pulseaudio") "/bin")))))
               #t))
           (add-after 'install 'make-desktop-entry
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (make-desktop-entry-file
                   (string-append out "/share/applications/beglitched.desktop")
                   #:name "Beglitched"
                   #:icon (assoc-ref inputs "icon")
                   #:exec (string-append out "/bin/beglitched")
                   #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("glu" ,glu)
         ("gtk+-2" ,gtk+-2)
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxext" ,libxext)
         ("libxi" ,libxi)
         ("libxinerama" ,libxinerama)
         ("libxrandr" ,libxrandr)
         ("libxscrnsaver" ,libxscrnsaver)
         ("libxxf86vm" ,libxxf86vm)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("zlib" ,zlib)))
      (native-inputs
       `(("unzip" ,unzip)
         ("icon" ,(origin
                    (method url-fetch)
                    (uri "https://img.itch.zone/aW1hZ2UvODQ4NDAvNDE2ODg0LmdpZg==/32x32%23m/MunGQk.gif")
                    (file-name "beglitched-icon.png")
                    (sha256
                     (base32
                      "1sk2q36ph1npmhhy5m8xjjj0lakssxk2x1h743nz2vrvkb856syk"))))))
      (home-page "https://hexecutable.itch.io/beglitched")
      (synopsis "Arcade game")
      (description "Beglitched is a game about insecurity, in our computers and
ourselves.  In a pastel world of networks where nobody truly knows what they're
doing, hacking is a magical art and the notorious Glitch Witch is the most
premium archmagi of the net.  By random circumstance, YOU are her new
apprentice.  You must use your wits and cunning to unravel the mechanisms of
an alien computer and survive amongst a veritable web of clowns, leftclickers,
and filedraggers.")
      (license (undistributable "No License")))))
