;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages torchlight2)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public gog-torchlight2
  (let ((gog-version "2.0.0.2")
        (binary (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "Torchlight2.bin.x86_64")
                  ("i686-linux" "Torchlight2.bin.x86")
                  (_ "")))
        (lib-dir (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "lib64")
                   ("i686-linux" "lib")
                   (_ "")))
        (other-lib-dir (match (or (%current-target-system)
                                  (%current-system))
                         ("x86_64-linux" "lib")
                         ("i686-linux" "lib64"))))
    (package
      (name "gog-torchlight2")
      (version "1.25.9.7")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://torchlight_ii/en3installer2")
         (file-name (string-append "gog_torchlight_2_"
                                   gog-version ".sh"))
         (sha256
          (base32
           "1669d1apmnv1fp1w6ksm383983l84nsz0svxrd195x93skvcfl5f"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "bzip2" "gcc:lib" "libstdc++" "libuuid" "mesa" "sdl2" "zlib"))
           (,,(string-append lib-dir "/libfmodex.so")
            ("gcc:lib" "libstdc++"))
           ;; The ogre package in Guix doesn't have this file.
           (,,(string-append lib-dir "/libOgreMain.so.1")
            ("bzip2" "freeimage" "freetype" "gcc:lib" "libstdc++" "sdl2" "zlib"))
           (,,(string-append lib-dir "/libCEGUIBase.so.1")
            ("freetype" "gcc:lib" "libstdc++" "sdl2"))
           (,,(string-append lib-dir "/libCEGUIExpatParser.so")
            ("expat" "gcc:lib" "libstdc++"))
           (,,(string-append lib-dir "/libCEGUIFalagardWRBase.so")
            ("gcc:lib" "libstdc++"))
           (,,(string-append lib-dir "/libCEGUIFreeImageImageCodec.so")
            ("freeimage" "gcc:lib" "libstdc++"))
           (,,(string-append lib-dir "/Plugin_OctreeSceneManager.so")
            ("bzip2" "freeimage" "freetype" "gcc:lib" "libstdc++" "mesa" "zlib"))
           (,,(string-append lib-dir "/RenderSystem_GL.so")
            ("bzip2" "freeimage" "freetype" "gcc:lib" "glu" "libice" "libsm"
             "libstdc++" "libxext" "libx11" "mesa" "sdl2" "zlib")))
         #:phases
         (modify-phases %standard-phases
           (add-before 'install 'delete-bundled-libs
             (lambda* (#:key outputs #:allow-other-keys)
               (with-directory-excursion "data/noarch/game"
                 (delete-file-recursively ,other-lib-dir)
                 (for-each (lambda (file)
                             (delete-file (string-append ,lib-dir file)))
                           '("/libfreeimage.so.3"
                             "/libfreetype.so.6"
                             "/libSDL2-2.0.so.0"))))))))
      (inputs
       `(("bzip2" ,bzip2)
         ("expat" ,expat)
         ("freeimage" ,freeimage)
         ("freetype" ,freetype)
         ("gcc:lib" ,gcc "lib")
         ("glu" ,glu)
         ("libice" ,libice)
         ("libsm" ,libsm)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("libuuid" ,util-linux "lib")
         ("libx11" ,libx11)
         ("libxext" ,libxext)
         ("mesa" ,mesa)
         ("sdl2" ,sdl2)
         ("zlib" ,zlib)))
      (home-page "https://www.torchlight2.com/")
      (synopsis "Action RPG dungeon crawler")
      (description "Torchlight II is an action role-playing dungeon crawler
game.  Featuring peer-to-peer co-op multiplayer, individual loot drops,
and a randomized game world.  Create and customize a character starting from
one of four available classes and choose an animal companion.")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
