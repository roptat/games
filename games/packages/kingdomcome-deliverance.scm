;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019, 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages kingdomcome-deliverance)
  #:use-module (games utils)
  #:use-module (nonguix licenses)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages wine)
  #:use-module (nongnu packages wine))

;; TODO: Use Guile instead?
(define make-wine-wrapper
  '(lambda* (file name #:key (game-path name))
     ;; From (guix utils).  TODO: Can we do shorter?
     (define* (string-replace-substring str substr replacement
                                        #:optional
                                        (start 0)
                                        (end (string-length str)))
       "Replace all occurrences of SUBSTR in the START--END range of STR by
REPLACEMENT."
       (match (string-length substr)
         (0
          (error "string-replace-substring: empty substring"))
         (substr-length
          (let loop ((start  start)
                     (pieces (list (substring str 0 start))))
            (match (string-contains str substr start end)
              (#f
               (string-concatenate-reverse
                (cons (substring str start) pieces)))
              (index
               (loop (+ index substr-length)
                     (cons* replacement
                            (substring str start index)
                            pieces))))))))
     ;; TODO: Use with-output-to-string.
     (define (environment-variables)
       (string-join
        '("XDG_DATA_HOME=${XDG_DATA_HOME:-$HOME/.local/share}"
          "GUIX_SAVE_PATH=${GUIX_SAVE_PATH:-$XDG_DATA_HOME}"
          "GUIX_GAMING_PATH=${GUIX_GAMING_PATH:-$HOME/Games}")
        (string #\newline)))
     (define (shell-name name)
       (string-replace-substring (string-upcase name) " " "_"))
     (define (file-name name)
       (string-replace-substring (string-downcase name) " " "-"))
     (define (wine-prefix name)
       (string-append (shell-name name) "_WINEPREFIX"))
     (define (save-path-variable name)
       (string-append (shell-name name) "_SAVE_PATH"))
     (define (game-path-variable name)
       (string-append (shell-name name) "_PATH"))
     (define (game-environment-variables name)
       (let ((shell-name (shell-name name))
             (file-name (file-name name))
             (wine-prefix (wine-prefix name))
             (save-path-variable (save-path-variable name))
             (game-path-variable (game-path-variable name)))
         (string-join
          (list (format #f "~a=${~a:-$GUIX_SAVE_PATH/~a}"
                        save-path-variable save-path-variable game-path)
                (format #f "~a=${~a:-$GUIX_GAMING_PATH/~a}"
                        game-path-variable game-path-variable game-path)
                (format #f "~a=${~a:-$HOME/.cache/~a}"
                        wine-prefix wine-prefix file-name))
          (string #\newline))))
     (define (dxvk-install prefix)
       ;; TODO: Detect installed version and install if mismatch.
       ;; TODO: Can we make a functional WINEPREFIX?
       (string-join
        (list (format #f "export WINEPREFIX=\"~a\"" prefix)
              "if [ ! -e \"$WINEPREFIX\" ]; then"
              "wine64 wineboot" ; TODO: Ungexp.
              ;; TODO: wineboot exits before the prefix is fully generated.
              ;; Can we avoid this loop?
              "while [ ! -e \"$WINEPREFIX/system.reg\" ]; do"
              "sleep 1"
              "done"
              "setup_dxvk install"      ; TODO: Ungexp.
              "fi")
        (string #\newline)))
     (define (link-saves from to)
       (string-join
        (list (format #f "mkdir -p \"~a\"" from)
              (format #f "ln -sfv \"~a\" \"~a\"" from to))
        (string #\newline)))

     (with-output-to-file file
       (lambda _
         (format #t "#!~a/bin/bash~%~%" (assoc-ref %build-inputs "bash")) ; TODO: Ungexp.
         (format #t "~a~%~%" (environment-variables))
         (format #t "~a~%~%" (game-environment-variables name))
         ;; Running the game generates a "Launcher.log" in the current directory.
         (format #t
                 (string-append "cd \"$~a\" || \\~%"
                                "  { echo >&2 \"'$~a' is not a folder\" && exit 1 ; }~%~%")
                 (game-path-variable name)
                 (game-path-variable name))
         (format #t
                 (string-append "[ -e Bin/Win64/d3dcompiler_46.dll ] || \\~%"
                                "  cp Bin/Win64Shared/d3dcompiler_46.dll Bin/Win64/d3dcompiler_46.dll~%"))
         (format #t (string-append "[ -e Bin/Win64/d3dcompiler_47.dll ] || \\~& "
                                   "  cp Bin/Win64Shared/d3dcompiler_47.dll Bin/Win64/d3dcompiler_47.dll~%~%"))
         ;; dxvk gives a huge performance boost.
         (format #t "~a~%~%" (dxvk-install (string-append "$" (wine-prefix name))))
         ;; Saves are buried deep within the prefix, let's dig them out:
         (format #t "~a~%~%" (link-saves
                              (string-append "$" (save-path-variable name))
                              "$WINEPREFIX/drive_c/users/$USER/Saved Games/kingdomcome"))
         ;; TODO: Ungexp wine.
         (format #t "exec wine64 Bin/Win64/KingdomCome.exe \"$@\"")))
     (chmod file #o755)))

;; TODO: Use wine-dxvk.
;; TODO: Containerize.
(define-public kingdomcome-deliverance
  (package
    (name "kingdomcome-deliverance")
    (version "0.0.0")
    (source #f)
    (build-system trivial-build-system)
    (inputs
     `(("bash" ,bash-minimal)
       ("wine" ,wine64-staging)         ; DXVK 1.7 does not seem to work with Wine 5.3.
       ("dxvk" ,dxvk-1.7)
       ("icon"
        ,(origin
           (method url-fetch)
           (uri "https://images.gog.com/4db3a5894306bf74e6f6d9dcbec42314b7a760de8a4af3bc633dec4ec1f714cb.png")
           (sha256
        (base32
         "0czcjz6a8s038i2zhajxs1dy4a2qzzcgnznxkfh5byppbf00iabj"))))))
    (arguments
     `(#:modules ((guix build utils)
                  (ice-9 match))
       #:builder
       (begin
         (use-modules (guix build utils)
                      (ice-9 match))
         (let* ((out (assoc-ref %outputs "out"))
                (bash (assoc-ref %build-inputs "bash"))
                (wine (assoc-ref %build-inputs "wine"))
                (dxvk (assoc-ref %build-inputs "dxvk"))
                (launcher (string-append out "/bin/kingdomcome-deliverance"))
                (make-wine-wrapper ,make-wine-wrapper)
                (icon-source (assoc-ref %build-inputs "icon"))
                (icon (string-append out "/share/kingdomcome-deliverance/"
                                     "KingdomComeDeliverance.png")))
           (mkdir-p (string-append out "/bin"))
           (make-wine-wrapper launcher "KingdomCome Deliverance"
                              #:game-path "Kingdom Come Deliverance")
           (patch-shebang launcher (list (string-append bash "/bin")))
           ;; TODO: Replace this substitute phase with g-exps.
           (substitute* launcher
             (("wine64") (string-append wine "/bin/wine64"))
             (("setup_dxvk") (string-append dxvk "/bin/setup_dxvk")))
           (mkdir-p (dirname icon))
           (copy-file icon-source icon)
           (make-desktop-entry-file
            (string-append out "/share/applications/kingdomcome-deliverance.desktop")
            #:name "Kingdom Come: Deliverance"
            #:exec launcher
            #:icon icon
            #:categories '("Application" "Game")))
         #t)))
    (home-page "https://www.kingdomcomerpg.com/")
    (synopsis "Story-driven open-world RPG, with historically accurate content")
    (description "Kingdom Come: Deliverance is a story-driven open-world RPG
that immerses you in an epic adventure in the Holy Roman Empire.

The game data must be installed.  It is looked for at the path pointed to by the
KINGDOMCOME_DELIVERANCE_PATH environment variable, which defaults to
$GUIX_GAMING_PATH/Kingdom Come Deliverance.

GUIX_GAMING_PATH defaults to \"~/Games\".

The saves are stored in KINGDOMCOME_DELIVERANCE_SAVE_PATH, which defaults to
\"GUIX_SAVE_PATH/Kingdom Come Deliverance\".

GUIX_SAVE_PATH defaults to XDG_DATA_HOME, which defaults to \"~/.local/share\".

The game uses Wine to run.  The Wine data is stored in
KINGDOMCOME_DELIVERANCE_WINEPREFIX, which defaults to
\"~/.cache/kingdomcome-deliverance\".")
    (supported-systems '("x86_64-linux"))
    (license (undistributable "No URL"))))
